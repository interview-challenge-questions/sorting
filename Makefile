MAIN := 'cmd/sorting/main.go'
BIN := 'sorting'

all: test build

test:
	go test -cover ./...

build:
	go build -o $(BIN) $(MAIN)

clean:
	go clean ./...
