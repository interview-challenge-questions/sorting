package sort

import (
	"reflect"
	"testing"
)

func TestBubble_Sort(t *testing.T) {
	type args struct {
		unsorted []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"unsorted",
			args{[]int{15, 8, 5, 9, 1, 2, 20}},
			[]int{1, 2, 5, 8, 9, 15, 20},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Bubble{}
			if got := b.Sort(tt.args.unsorted); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Sort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInsertion_Sort(t *testing.T) {
	type args struct {
		unsorted []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"unsorted",
			args{[]int{15, 8, 5, 9, 1, 2, 20}},
			[]int{1, 2, 5, 8, 9, 15, 20},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ins := Insertion{}
			if got := ins.Sort(tt.args.unsorted); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Sort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSort(t *testing.T) {
	type args struct {
		algName string
		input   []int
	}
	tests := []struct {
		name       string
		args       args
		wantOutput []int
		wantErr    bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotOutput, err := Sort(tt.args.algName, tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("Sort() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOutput, tt.wantOutput) {
				t.Errorf("Sort() gotOutput = %v, want %v", gotOutput, tt.wantOutput)
			}
		})
	}
}
