package sort

import (
	"fmt"
)

type Algorithm interface {
	Sort(unsorted []int) []int
}

type Bubble struct{}

type Insertion struct{}

func Sort(algName string, input []int) (output []int, err error) {
	switch alg := algName; alg {
	case "bubble":
		output = Bubble{}.Sort(input)
	case "insertion":
		output = Insertion{}.Sort(input)
	default:
		err = fmt.Errorf("Algorithm \"%s\" not available\n", algName)
	}

	return
}

func (b Bubble) Sort(unsorted []int) []int {
	input := make([]int, len(unsorted))
	copy(input, unsorted)

	maxIdx := len(input) - 1
	var isSorted bool

	for {
		isSorted = true

		for idx, i := range input {
			if idx == maxIdx {
				break
			}

			if input[idx] > input[idx+1] {
				isSorted = false

				input[idx] = input[idx+1]
				input[idx+1] = i
			}
		}

		if isSorted {
			break
		}
	}

	return input
}

func (ins Insertion) Sort(unsorted []int) []int {
	input := make([]int, len(unsorted))
	copy(input, unsorted)

	for idx, i := range input {
		if idx == 0 {
			continue
		}

		for j := idx; j > 0; j-- {
			if input[j] < input[j-1] {
				input[j] = input[j-1]
				input[j-1] = i
			}
		}
	}

	return input
}
