package serve

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sorting/pkg/sort"
)

type values struct {
	Values []int `json:"values"`
}

func Equal() {

}

func Handle() {
	http.HandleFunc("/sort", Sort)
}

func Sort(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		q := r.URL.Query()

		algName := q.Get("algorithm")

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf(err.Error())
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		var input = values{}

		err = json.Unmarshal(body, &input)
		if err != nil {
			log.Printf(err.Error())
		}

		outputArray, err := sort.Sort(algName, input.Values)
		if err != nil {
			fmt.Printf("%s\n", err)
		}

		output := values{outputArray}

		fmt.Printf("Output: %+v\n", outputArray)

		b, err := json.Marshal(output)

		if err != nil {
			log.Printf(err.Error())
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		_, err = w.Write(b)
		if err != nil {
			log.Printf(err.Error())
			w.WriteHeader(http.StatusInternalServerError)

			return
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}
