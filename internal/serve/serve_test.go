package serve

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandle(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func TestSort(t *testing.T) {
	v := values{
		Values: []int{15, 8, 5, 9, 1, 2, 20},
	}
	body, err := json.Marshal(v)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/sort?algorithm=bubble", bytes.NewBuffer(body))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Sort)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := values{
		Values: []int{1, 2, 5, 8, 9, 15, 20},
	}

	var actual = values{}
	err = json.Unmarshal(rr.Body.Bytes(), &actual)
	if err != nil {
		t.Fatal(err)
	}

	if actual.Values != expected.Values {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
