package main

import (
	"fmt"
	"github.com/spf13/pflag"
	"log"
	"net/http"
	"sorting/internal/serve"
	"sorting/pkg/sort"
)

func main() {
	serverPort := "8080"

	isServer := pflag.BoolP(
		"server",
		"s",
		false,
		fmt.Sprintf("start the application as an HTTP server on port %s", serverPort),
	)

	unsorted := pflag.IntSliceP(
		"unsorted",
		"u",
		[]int{},
		"unsorted values to sort",
	)

	// TODO: List available algorithms
	algName := pflag.StringP(
		"algorithm",
		"a",
		"",
		"name of the sorting algorithm to use",
	)

	pflag.Parse()

	if *isServer {
		serve.Handle()

		log.Fatal(
			http.ListenAndServe(fmt.Sprintf(":%s", serverPort), nil))
	}

	output, err := sort.Sort(*algName, *unsorted)
	if err != nil {
		log.Fatalf("%s\n", err)
	}

	fmt.Printf("Output: %+v\n", output)
}
