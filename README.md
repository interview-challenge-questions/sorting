# sorting

A simple proof-of-concept application to sort integers through a CLI or RESTful HTTP
interface.

## Build
```shell script
$ make
```

## Run
### CLI
Run the binary with the integers to be sorted and the algorithm to use:
```shell script
$ ./sorting --unosrted 4,8,2,1 --algorithm bubble
```

### HTTP
Start the server:
```shell script
$ ./sorting --server
```
This listens on port `8080` for requests.

Now send JSON containing the integers to be sorted to the server and specify the algorithm to be used:
```shell script
$ curl -X POST -d '{ "values": [ 15, 8, 5, 9, 1, 2, 20 ] }' localhost:8080/sort?algorithm=bubble
```

This will return JSON with the sorted integers:
```json
{
  "values": [
    1,
    2,
    5,
    8,
    9,
    15,
    20
  ]
}
```
